<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

        <title>Biblioteca</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
       <link href="{{ asset('css/principal.css') }}" rel="stylesheet">
       @yield('estilos')
    </head>
    <body>
        <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="collapse navbar-collapse" id="Biblioteca">
                    <ul class="navbar-nav mr-auto">

                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('home') }}">Home<span class="sr-only">(página atual)</span></a>
                        </li>     

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarListagem" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Listagem
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarListagem">
                                <a class="dropdown-item" href="{{ route('listagemAutores') }}">Autores</a>
                                <a class="dropdown-item" href="{{ route('listagemEditoras') }}">Editoras</a>
                                <a class="dropdown-item" href="{{ route('listagemLivros') }}">Livros</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Algo mais aqui</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Algo mais aqui 2</a>
                                <a class="dropdown-item" href="#">Algo mais aqui 3</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarEditar" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Editar
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarEditar">
                                <a class="dropdown-item" href="{{ route('editarAutores') }}">Autores</a>
                                <a class="dropdown-item" href="{{ route('editarEditoras') }}">Editoras</a>
                                <a class="dropdown-item" href="{{ route('editarLivros') }}">Livros</a>
                            </div>
                        </li>
                    </ul>
                </div> 
            </nav>       
            <!--

        <div class="nav-scroller py-1 mb-2">
            <nav class="nav d-flex justify-content-between">
                <a class="p-2 text-muted" href="{{ route('home') }}">Home</a>
                <a class="p-2 text-muted" href="{{ route('listagemLivros') }}">Listagem Livros</a>
                <a class="p-2 text-muted" href="{{ route('listagemAutores') }}">Listagem Autores</a>
                <a class="p-2 text-muted" href="{{ route('listagemEditoras') }}">Listagem Editoras</a>
                <a class="p-2 text-muted" href="{{ route('editarAutores') }}">Editar Autores</a>
                <a class="p-2 text-muted" href="{{ route('editarEditoras') }}">Editar Editoras</a>
                <a class="p-2 text-muted" href="{{ route('editarLivros') }}">Editar Livros</a>
            </nav>
        </div>
        -->
        @yield('conteudo')
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
        @yield('scripts')
    </body>
</html>