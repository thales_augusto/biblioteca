@extends('templates.template_base')
@section('estilos')
<script src="https://kit.fontawesome.com/d38b5056d1.js" crossorigin="anonymous"></script>
    <style type="text/css">
        .titulo{
                background-color:rgb(246, 233, 233);
                color: #0a1b12;
                margin-bottom: ;
        }
        .btn-custom {
            padding:1px 15px 3px 2px;
            border-radius:50px;
        }
        .btn-icon {
            padding:8px;
        }
    </style>
@endsection
@section('conteudo')
        <div class="row">
            <div class="col-md-8 titulo">
                <h2 class="">Edição de: {{ $autor->nome }}</h2>
            </div>
        </div>
        <hr>
        <form action="{{url('salva_autor')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$autor->id}}">
        <div class="form-row">
            <div class="form-group col-md-2">
                <label for="inputNome">Nome</label>
                <input type="text" class="form-control" name="nome" value="{{$autor->nome}}">
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Salvar</button>    
        </form>
@endsection
