@extends('templates.template_base')
@section('estilos')
<script src="https://kit.fontawesome.com/d38b5056d1.js" crossorigin="anonymous"></script>
    <style type="text/css">
        .titulo{
                background-color:rgb(246, 233, 233);
                color: #0a1b12;
                margin-bottom: ;
        }
        .btn-custom {
            padding:1px 15px 3px 2px;
            border-radius:50px;
        }
        .btn-icon {
            padding:8px;
        }
    </style>
@endsection
@section('conteudo')
        <div class="row">
            <div class="col-md-8 titulo">
                <h2 class="">Edição de: {{ $livro->titulo }}</h2>
            </div>
        </div>
        <hr>
        <form action="{{url('salva_livro')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$livro->id}}">
        <div class="form-row">
            <div class="form-group col-md-2">
                <label for="inputTil">Titulo</label>
                <input type="text" class="form-control" name="titulo" value="{{$livro->titulo}}">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="inputAut">Autor</label>
                <select name="id_autor" class="form-control">
                    @foreach ($autores as $autor)
                        @if ($livro->id_autor == $autor->id)
                            <option value="{{ $autor->id}}" selected >{{ $autor->nome }}</option>
                        @else
                            <option value="{{ $autor->id }}" >{{ $autor->nome }}</option>
                        @endif
                    @endforeach        
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="inputEdi">Editora</label>
                <select name="id_editora" class="form-control">
                    @foreach ($editoras as $editora)
                        @if ($livro->id_editora == $editora->id)
                            <option value="{{ $editora->id}}" selected >{{ $editora->nome }}</option>
                        @else
                            <option value="{{ $editora->id }}" >{{ $editora->nome }}</option>
                        @endif
                    @endforeach        
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-2">
                <label for="inputISBN">ISBN</label>
                <input type="text" class="form-control" name="isbn" value="{{ $livro->isbn }}"> 
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-2">
                <label for="inputLoc">Local</label>
                <input type="text" class="form-control" name="local" value="{{ $livro->local }}"> 
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Salvar</button>    
        </form>
@endsection
