@extends('templates.template_base')
@section('estilos')
<style type="text/css">
    img {
        max-width: 50%;
        height: auto;
        border-radius: 50%;
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
</style>
@endsection

@section('conteudo')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-auto">
                <h1> Biblioteca - Thales </h1>
                <hr>
                <h2> Aluno: Thales Augusto Silva Leite</h2>
                <h2> Turma: AUT2D2 </h2>
            </div>
            <div class="row">
                <img src="{{ asset('imgs/BIBLIO44.png')}}">
            </div>
        </div>
        <div class="text-center mark">
            v - {{ App::VERSION()}}
        </div>

    </div>
@endsection    
