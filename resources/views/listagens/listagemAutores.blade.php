@extends('templates.template_base')
@section('estilos')
@endsection

@section('conteudo')
    <div class="row">
        <div class="col-md-4">
            <h2> Listagem de Autores </h2>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <table class="table table-striped">
                    <tr>
                        <td> Id</td>
                        <td> Nome</td>
                    </tr>
                    @foreach ($autores as $autor)
                        <tr>
                            <td>{{ $autor->id }}</td>
                            <td>{{ $autor->nome }}</td>
                        </tr>
                    @endforeach
                </table>    
            </div>
        </div>
    </div>
@endsection    