@extends('templates.template_base')

@section('conteudo')
    <div class="row">
        <div class="col-md-4">
                <h2> Listagem de Livros </h2>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-8">
                <table class="table table-striped">
                    <tr>
                        <td> Título</td>
                        <td> Autor</td>
                        <td> Editora</td>
                        <td> ISBN</td>
                        <td> Local</td>
                    </tr>
                    @foreach ($livros as $livro)
                        <tr>
                            <td>{{ $livro->titulo }}</td>
                            <td>{{ $livro->autor->nome }}</td>
                            <td>{{ $livro->editora->nome }}</td>
                            <td>{{ $livro->isbn }}</td>
                            <td>{{ $livro->local }}</td>
                        </tr>
                    @endforeach
                </table>    
            </div>
        </div>
    </div>
@endsection 

@section('scripts')
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
@endsection