@extends('templates.template_base')

@section('conteudo')
    <div class="row">
        <div class="col-md-4">
            <h2> Listagem de Editoras </h2>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <table class="table table-striped">
                    <tr>
                        <td> Id</td>
                        <td> Nome</td>
                    </tr>
                    @foreach ($editoras as $editora)
                        <tr>
                            <td>{{ $editora->id }}</td>
                            <td>{{ $editora->nome }}</td>
                        </tr>
                    @endforeach
                </table>    
            </div>
        </div>
    </div>
@endsection