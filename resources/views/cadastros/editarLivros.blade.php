@extends('templates.template_base')
@section('estilos')
<script src="https://kit.fontawesome.com/d38b5056d1.js" crossorigin="anonymous"></script>
    <style type="text/css">
        .btn-custom {
            padding:1px 15px 3px 2px;
            border-radius:50px;
        }
        .btn-icon {
            padding:8px;
        }
    </style>
@endsection

@section('conteudo')
    <div class="row">
        <div class="col-md-4">
                <h2> Edição de Livros </h2>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-8">
                <table class="table table-striped">
                    <tr>
                        <td> Título</td>
                        <td> Autor</td>
                        <td> Editora</td>
                        <td> ISBN</td>
                        <td> Local</td>
                        <td> Ações</td>
                    </tr>
                    @foreach ($livros as $livro)
                        <tr>
                            <td>{{ $livro->titulo }}</td>
                            <td>{{ $livro->autor->nome }}</td>
                            <td>{{ $livro->editora->nome }}</td>
                            <td>{{ $livro->isbn }}</td>
                            <td>{{ $livro->local }}</td>
                            <td>
                                <button id="{{ $livro->id }}" class="btn btn-info btn-custom btEditar">
                                <span class="fas fa-pen btn-icon"></span>
                                    Editar
                                </button>  
                                <button id="{{ $livro->id }}" class="btn btn-danger btn-custom btDeletar">
                                <span class="fas fa-trash btn-icon"></span>
                                    Apagar
                                </button> 
                            </td>
                        </tr>
                    @endforeach
                </table>    
            </div>
        </div>
    </div>
    <div class="row">
        <button id="btInserir" class="btn btn-primary btn-custom">
        <span class="fas fa-plus-circle btn-icon"></span>
            Inserir Novo
        </button>    
    </div> 
@endsection 

@section('scripts')
        <script>
            $('.btEditar').click(function(){
                var id = $(this).attr('id');
                var url = "{{url('/')}}"+'/editar_livro/'+id;
                alert(url);
                window.location.href = url;
            });
            
            $('#btInserir').click(function(){
                var url = "{{url('inserir_novo_livro')}}";
                alert(url);
                window.location.href = url;
            });

            $('.btDeletar').click(function(){
                var id = $(this).attr('id');
                var resp = confirm("Deseja excluir o Livro Id: " + id + " ?");
                if(resp){
                    var url = "{{url('/')}}"+'/deletar_livro/'+id;
                    alert(url);
                    window.location.href = url;
                }
            });
        </script>
@endsection