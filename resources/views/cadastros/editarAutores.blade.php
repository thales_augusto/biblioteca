@extends('templates.template_base')
@section('estilos')
<script src="https://kit.fontawesome.com/d38b5056d1.js" crossorigin="anonymous"></script>
    <style type="text/css">
        .btn-custom {
            padding:1px 15px 3px 2px;
            border-radius:50px;
        }
        .btn-icon {
            padding:8px;
        }
    </style>
@endsection

@section('conteudo')
    <div class="row">
        <div class="col-md-4">
            <h2> Edição de Autores </h2>
        </div>
        <hr>
        @if($message = Session::get('error'))
            <div class="alert alert-warning alert-block">
                <button type="button" class="close" data-dismiss="alert">X</button>
                {!! $message !!}
            </div>
        @endif
        <div class="row">
            <div class="col-md-6">
                <table class="table table-striped">
                    <tr>
                        <td> Id</td>
                        <td> Nome</td>
                        <td> Ações</td>
                    </tr>
                    @foreach ($autores as $autor)
                        <tr>
                            <td>{{ $autor->id }}</td>
                            <td>{{ $autor->nome }}</td>
                            <td>
                                <button id="{{ $autor->id }}" class="btn btn-info btn-custom btEditar">
                                <span class="fas fa-pen btn-icon"></span>
                                    Editar
                                </button>  
                                <button id="{{ $autor->id }}" class="btn btn-danger btn-custom btDeletar">
                                <span class="fas fa-trash btn-icon"></span>
                                    Apagar
                                </button> 
                            </td>
                        </tr>
                    @endforeach
                </table>    
            </div>
        </div>
    </div>
    <div class="row">
        <button id="btInserir" class="btn btn-primary btn-custom">
        <span class="fas fa-plus-circle btn-icon"></span>
            Inserir Novo
        </button>    
    </div>
@endsection    
@section('scripts')
        <script>
            $('.btEditar').click(function(){
                var id = $(this).attr('id');
                var url = "{{url('/')}}"+'/editar_autor/'+id;
                alert(url);
                window.location.href = url;
            });

            $('#btInserir').click(function(){
                var url = "{{url('inserir_novo_autor')}}";
                alert(url);
                window.location.href = url;
            });

            $('.btDeletar').click(function(){
                var id = $(this).attr('id');
                var resp = confirm("Deseja excluir o Autor Id: " + id + " ?");
                if(resp){
                    var url = "{{url('/')}}"+'/deletar_autor/'+id;
                    alert(url);
                    window.location.href = url;
                }
            });
        </script>
@endsection