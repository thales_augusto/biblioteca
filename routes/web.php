<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'paginasController@index')->name('home');

Route::get('/listagemLivros','paginasController@listagemLivros')->name('listagemLivros');
Route::get('/listagemAutores','paginasController@listagemAutores')->name('listagemAutores');
Route::get('/listagemEditoras','paginasController@listagemEditoras')->name('listagemEditoras');

Route::get('/editarAutores','editarController@editarAutores')->name('editarAutores');
Route::get('/editarEditoras','editarController@editarEditoras')->name('editarEditoras');
Route::get('/editarLivros','editarController@editarLivros')->name('editarLivros');

Route::get('/editar_autor/{id}','editarController@editar_autor')->name('editar_autor');
Route::post('/salva_autor','editarController@salva_autor')->name('salva_autor');

Route::get('/editar_editora/{id}','editarController@editar_editora')->name('editar_editora');
Route::post('/salva_editora','editarController@salva_editora')->name('salva_editora');

Route::get('/editar_livro/{id}','editarController@editar_livro')->name('editar_livro');
Route::post('/salva_livro','editarController@salva_livro')->name('salva_livro');

// inserir e deletar AUTOR
Route::get('/inserir_novo_autor','editarController@inserir_novo_autor')->name('inserir_novo_autor');
Route::get('/deletar_autor/{id}','editarController@deletar_autor')->name('deletar_autor');

// inserir e deletar EDITORA
Route::get('/inserir_nova_editora','editarController@inserir_nova_editora')->name('inserir_nova_editora');
Route::get('/deletar_editora/{id}','editarController@deletar_editora')->name('deletar_editora');

// inserir e deletar LIVRO
Route::get('/inserir_novo_livro','editarController@inserir_novo_livro')->name('inserir_novo_livro');
Route::get('/deletar_livro/{id}','editarController@deletar_livro')->name('deletar_livro');