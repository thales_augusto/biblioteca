<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Livros extends Model
{
    public function autor()
    {
        return $this->belongsTo('App\Models\Autores', 'id_autor', 'id');
    }

    public function editora()
    {
        return $this->belongsTo('App\Models\Editoras', 'id_editora', 'id');
    }
}
