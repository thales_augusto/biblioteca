<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class editarController extends Controller
{
    public function editarAutores(){
        $autores = \App\Models\Autores::get();
        return view('cadastros/editarAutores')->with(compact('autores'));

    }
    public function editarEditoras(){
        $editoras = \App\Models\Editoras::get();
        return view('cadastros/editarEditoras')->with(compact('editoras'));

    }
    public function editarLivros(){
        $livros = \App\Models\Livros::get();
        return view('cadastros/editarLivros')->with(compact('livros'));

    }

    public function editar_autor($id){
        $autor = \App\Models\Autores::find($id);
        if($autor){
            return view('Editar/EditarAutor')->with(compact('autor'));
        }else{
            alert('erro ao acessar lançamento!');
            return redirect()->back()->withInput();
        }

    }
    public function salva_autor(Request $request){
        //var dump($request->all());
        //exit;
        $autor = \App\Models\Autores::find($request->id); 
        $autor->nome = $request->nome;
        $autor->save();
        return redirect('editarAutores');

    }

    public function editar_editora($id){
        $editora = \App\Models\Editoras::find($id);
        if($editora){
            return view('Editar/EditarEditora')->with(compact('editora'));
        }else{
            alert('erro ao acessar lançamento!');
            return redirect()->back()->withInput();
        }
    }
    public function salva_editora(Request $request){
        //var dump($request->all());
        //exit;
        $editora = \App\Models\Editoras::find($request->id); 
        $editora->nome = $request->nome;
        $editora->save();
        return redirect('editarEditoras');
    } 
    
    public function editar_livro($id){
        $livro = \App\Models\Livros::find($id);
        $autores = \App\Models\Autores::get();
        $editoras = \App\Models\Editoras::get();
        if($livro){
            return view('Editar/EditarLivro')->with(compact('livro','autores','editoras'));
        }else{
            alert('erro ao acessar lançamento!');
            return redirect()->back()->withInput();
        }
    }
    public function salva_livro(Request $request){
        //var dump($request->all());
        //exit;
        $livro = \App\Models\Livros::find($request->id); 
        $livro->titulo = $request->titulo;
        $livro->id_autor = $request->id_autor;
        $livro->id_editora = $request->id_editora;
        $livro->isbn = $request->isbn;
        $livro->local = $request->local;
        $livro->save();
        return redirect('editarLivros');

    } 

    // Inserir e Deletar AUTOR
    public function inserir_novo_autor(){
        $autor = new \App\Models\Autores;
        $autor->nome = 'novo';
        $autor->save();

        return redirect ('editar_autor/'.$autor->id);
    }
    public function deletar_autor($id){
        $livro = \App\Models\Livros::where('id_autor',$id)->first();
        if ($livro){
            return redirect('editarAutores')->with('error',"Existe livro cadastrado para autor id: ".$id.".autor NÃO pode ser deletado!");
        }else{
            $autor = \App\Models\Autores::find($id);
            $autor->delete();
            return redirect('editarAutores');
        }

    }

    // Inserir e Deletar EDITORA
    public function inserir_nova_editora(){
        $editora = new \App\Models\Editoras;
        $editora->nome = 'novo';
        $editora->save();

        return redirect ('editar_editora/'.$editora->id);
    }
    public function deletar_editora($id){
        $livro = \App\Models\Livros::where('id_editora',$id)->first();
        if ($livro){
            return redirect('editarEditora')->with('error',"Existe livro cadastrado para editora id: ".$id.".editora NÃO pode ser deletado!");
        }else{
            $editora = \App\Models\Editoras::find($id);
            $editora->delete();
            return redirect('editarEditoras');
        }

    }

    // Inserir e Deletar LIVRO
    public function inserir_novo_livro(){
        $livro = new \App\Models\Livros;
        $livro->titulo = 'novo';
        $livro->save();

        return redirect ('editarLivros/'.$livro->id);
    }
    public function deletar_livro($id){
        $livro = \App\Models\Livros::where('id',$id)->first();
        if ($livro){
            $livro = \App\Models\Livros::find($id);
            $livro->delete();
            return redirect('editarLivros');
        }

    }
}
