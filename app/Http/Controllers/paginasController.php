<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class paginasController extends Controller
{
    public function index(){
        return view('Biblioteca_Thales');
    }

    public function listagemLivros(){
        $livros = \App\Models\Livros::with('autor')->get();
        return view('listagens\listagemLivros')->with(compact('livros'));
    }

    public function listagemAutores(){
        $autores = \App\Models\Autores::get();
        return view('listagens\listagemAutores')->with(compact('autores'));
    }

    public function listagemEditoras(){
        $editoras = \App\Models\Editoras::get();
        return view('listagens\listagemEditoras')->with(compact('editoras'));
    }
}
